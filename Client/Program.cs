﻿using System.Data.SQLite;
using System.Collections.Generic;
using CsharpAes;
using System.Messaging;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

class Program
{
    static PasswordAes aes = new PasswordAes("myCryptoKey");
    static void Main(string[] args)
    {
        bool hello = false;
        while (true)
        {
            if (!hello)
            {
                
                Console.WriteLine("msmq - to send via msmq");
                Console.WriteLine("socket - to send via soclets");
                Console.WriteLine("exit - to shutdown client");
                Console.WriteLine("everything else would be ignored");
                hello = true;
            }
            Console.WriteLine("Please enter some command");
            string operation = Console.ReadLine();
            if (operation == "exit")
                break;
            if (operation == "msmq")
                MSMQ();
            if (operation == "socket")
                Socket();
        }
    }

    static string encryptData()
    {
        var data = GetDatabaseRows();
        string result = aes.Encrypt(data);
        return result;
    }

    static List<smallDB.smallDB> GetDatabaseRows()
    {
        var result = new List<smallDB.smallDB>();

        var connection = new SQLiteConnection("data source=database.sqlite");
        connection.Open();

        var command = new SQLiteCommand("SELECT * FROM frisbeeplayer", connection);
        using (var reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                var newElement = new smallDB.smallDB
                {
                    playerName = reader.GetString(1),
                    teamName = reader.GetString(2),
                    teamCityName = reader.GetString(3),
                    prefThrowName = reader.GetString(4),
                    throwTypeName = reader.GetString(5),
                };

                result.Add(newElement);
            }
        }

        return result;
    }

    static void MSMQ()
    {
        //sending
        string toSend = encryptData();
        MessageBox.Show("Sending data using MSMQ");
        Console.WriteLine("IsLocal? y/n");
        MessageQueue queue;
        string ans = Console.ReadLine();
        if (ans == "y")
        {
            queue = new MessageQueue(@".\private$\MyQueue");
        }
        else
        {
            Console.WriteLine("ip?");
            string ip = Console.ReadLine();
            queue = new MessageQueue("FormatName:Direct=TCP:" + ip + @"\private$\MyQueue");
        }
        System.Messaging.Message msg = new System.Messaging.Message(toSend, new BinaryMessageFormatter());
        queue.Send(msg);
        MessageBox.Show("Data was successfully sent");
    }

    static void Socket()
    {
        //connection
        TcpClient client = new TcpClient();
        string ip = "";
        Console.WriteLine("Enter desired ip");
        ip = Console.ReadLine();
        Console.WriteLine("Enter desired port");
        string port = "";
        port = Console.ReadLine();
        client.Connect(ip, Convert.ToInt32(port));
        var stream = client.GetStream();

        //sending
        string toSend = encryptData();
        MessageBox.Show("Sending data using sockets");
        byte[] bytes = Encoding.Unicode.GetBytes(toSend);
        stream.Write(bytes, 0, bytes.Length);
        stream.Close();
        MessageBox.Show("Data was successfully sent");
    }
}