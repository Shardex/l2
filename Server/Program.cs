﻿using System.Collections.Generic;
using System.Messaging;
using CsharpAes;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using System.IO;
using System.Threading;
using MySql.Data.MySqlClient;

class Program
{
    static PasswordAes aes = new PasswordAes("myCryptoKey");

    static void Main(string[] args)
    {
        Thread mqT = new Thread(MSMQThread);
        mqT.Start();
        Thread scktT = new Thread(SocketThread);
        scktT.Start();

        Console.WriteLine("Waiting for something...");
        Console.ReadKey();

        scktT.Abort();
        mqT.Abort();
    }
    public static void MSMQThread()
    {
        RowsInserter inserter = new RowsInserter();
        string serverName = ".";
        string path = serverName + @"\private$\MyQueue";
        try
        {
            MessageQueue.Create(path);
        }
        catch 
        {

        }
        MessageQueue queue = new MessageQueue(path);
        queue.Formatter = new BinaryMessageFormatter();
        while (true)
        {
            var msg = queue.Receive();
            var decrypyedData = aes.Decrypt<List<smallDB.smallDB>>(msg.Body as string);
            Console.WriteLine("Recieved something from MSMQ");
            inserter.AddRows(decrypyedData);
        }
    }
    public static void SocketThread()
    {
        RowsInserter inserter = new RowsInserter();
        TcpListener listener = new TcpListener(IPAddress.Any, 20000);
        listener.Start(0);

        while(true)
        {
            var client = listener.AcceptTcpClient();
            var stream = client.GetStream();

            byte[] readBuffer = new byte[client.ReceiveBufferSize];
            string fullServerReply = null;
            using (var writer = new MemoryStream())
            {
                while (true)
                {
                    int numberOfBytesRead = stream.Read(readBuffer, 0, readBuffer.Length);
                    if (numberOfBytesRead <= 0) {
                        break;
                    }
                    writer.Write(readBuffer, 0, numberOfBytesRead);
                }
                fullServerReply = Encoding.Unicode.GetString(writer.ToArray());
            }

            var decrypyedData = aes.Decrypt<List<smallDB.smallDB>>(fullServerReply);
            Console.WriteLine("Recieved something from sockets");

            inserter.AddRows(decrypyedData);
        }
    }

    class RowsInserter : IDisposable
    {
        MySqlConnection con;

        public RowsInserter()
        {
            var conString = new MySqlConnectionStringBuilder();
            conString.Server = "localhost";
            conString.UserID = "root";
            conString.Password = "1234";
            conString.Database = "organization";

            con = new MySqlConnection(conString.ToString());
            con.Open();
        }

        public void Dispose() =>
            con.Close();

        public void AddRows(List<smallDB.smallDB> rows) =>
            rows.ForEach(x => ParseRows(con, x));

        void ParseRows(MySqlConnection con, smallDB.smallDB row)
        {
            //1-city 2-team 3-throwType 4-throw 5-player
            //Inserting city
            var comm = new MySqlCommand($"SELECT id FROM city WHERE name = '{row.teamCityName}' LIMIT 1", con);
            var cityId = comm.ExecuteScalar();
            //If there is no such city id - lets create
            if (cityId == null)
            {
                comm = new MySqlCommand($"INSERT INTO city (name) VALUES ('{row.teamCityName}')", con);
                comm.ExecuteNonQuery();
                cityId = comm.LastInsertedId;
            }
            else
            {
                cityId = int.Parse(cityId.ToString());
            }

            comm = new MySqlCommand($"SELECT id FROM team WHERE name = '{row.teamName}' AND cityId = {cityId} LIMIT 1", con);
            var teamId = comm.ExecuteScalar();

            if (teamId == null)
            {
                comm = new MySqlCommand($"INSERT INTO team (name, cityId) VALUES ('{row.teamName}', {cityId})", con);
                comm.ExecuteNonQuery();
                teamId = comm.LastInsertedId;
            }
            else
            {
                teamId = int.Parse(teamId.ToString());
            }
            comm = new MySqlCommand($"SELECT id FROM throwtype WHERE name = '{row.throwTypeName}' LIMIT 1", con);
            var throwTypeId = comm.ExecuteScalar();

            if (throwTypeId == null)
            {
                comm = new MySqlCommand($"INSERT INTO throwtype (name) VALUES ('{row.throwTypeName}')", con);
                comm.ExecuteNonQuery();
                throwTypeId = comm.LastInsertedId;
            }
            else
            {
                throwTypeId = int.Parse(throwTypeId.ToString());
            }

            comm = new MySqlCommand($"SELECT id FROM preferablethrow WHERE name = '{row.prefThrowName}' AND throwTypeId = {throwTypeId} LIMIT 1", con);
            var prefThrowId = comm.ExecuteScalar();
            if (prefThrowId == null)
            {
                comm = new MySqlCommand($"INSERT INTO preferablethrow (name, throwTypeId) VALUES ('{row.prefThrowName}', {throwTypeId})", con);
                comm.ExecuteNonQuery();

                prefThrowId = comm.LastInsertedId;
            }
            else
            {
                prefThrowId = int.Parse(prefThrowId.ToString());
            }
            comm = new MySqlCommand($"INSERT INTO frisbeeplayer (teamId, prefThrowId, name) VALUES ({teamId}, {prefThrowId}, '{row.playerName}')", con);
            comm.ExecuteNonQuery();
        }
    }
}
