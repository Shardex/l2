﻿using System;
using System.Runtime.Serialization;

namespace smallDB
{
    [Serializable]
    public class smallDB
    {
        public string playerName;
        public string teamCityName;
        public string teamName;
        public string prefThrowName;
        public string throwTypeName;

        public override string ToString()
        {
            return playerName;
        }
    }
}
